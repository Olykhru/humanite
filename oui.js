document.addEventListener('DOMContentLoaded', ()=>
{
  const DIRECT = new Date("Wed Apr 02 2025 14:00:00 UTC+2"),
    JOURS = document.querySelector("#jours"),
    HEURES = document.querySelector("#heures"),
    MINUTES = document.querySelector("#minutes"),
    SECONDES = document.querySelector("#secondes"),
    BG = document.querySelector("#BG"),
    INLINE = document.querySelectorAll(".inline"),
    TIMER = function()
    {
      let now = new Date(),
        diff = DIRECT - now.getTime();

      d = 0;
      h = 0;
      m = 0;
      s = 0;

      d = Math.floor(diff / (1000 * 60 * 60 * 24));
      diff -= d * 1000 * 60 * 60 * 24;
      h = Math.floor(diff / (1000 * 60 * 60));
      diff -= h * 1000 * 60 * 60;
      m = Math.floor(diff / (1000 * 60));
      diff -= m * 1000 * 60;
      s = Math.floor(diff / (1000));

      let percent = Math.round((d + h / 24 + m / 60 + s / 3600) / 76 * 100, 2);

      JOURS.innerText = d > 0 ? d + " jours " : ""
      HEURES.innerText = d > 0 || h > 0 ? h + " heures " : ""
      MINUTES.innerText = d > 0 || h > 0 || m > 0 ? m + " minutes et " : ""
      SECONDES.innerText = d > 0 || h > 0 || m > 0 || s > 0 ? s + " secondes" : "Maintenant ?"

      INLINE[0].style.width = (30 + .3 * percent) + "vw";
      INLINE[1].style.width = (30 + .3 * percent) + "vw";
    },
    RESIZE = ()=>
    {
      INLINE[0].style.backgroundPosition = "top 50% left " + ((document.body.clientWidth * 1.1 - 640) * 0.6) + "px";
      INLINE[1].style.backgroundPosition = "top 50% right " + ((document.body.clientWidth * 1.1 - 640) * 0.6) + "px";
    };

  let d = 0,
    h = 0,
    m = 0,
    s = 0;

  addEventListener("resize", (e) => {
    RESIZE();
  });

  setInterval(TIMER, 200);
  RESIZE();
});